import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class AutomataTest {
    @Test
    public void testOn() throws Exception {
        Automata obj = new Automata();
        obj.on();
        assertSame("WAIT", obj.on());
    }

    @Test
    public void testOff() throws Exception {
        Automata obj = new Automata();
        assertSame("OFF", obj.off());
    }

    @Test
    public void testMoney() throws Exception {
        Automata obj = new Automata();
        assertTrue(obj.money() > 0);
    }

    @Test
    public void testCoin() throws Exception {
        Automata obj = new Automata();
        obj.on();
        assertEquals(0, obj.coin(10));
    }

    @Test
    public void printMenu() throws Exception {
        Automata obj = new Automata();
        obj.on();
        assertEquals(0, obj.PrintMenu());
    }

    @Test
    public void printState() throws Exception {
        Automata obj = new Automata();
        try {
            obj.on();
            obj.printState();
        }
        catch (Exception e){
            System.out.println("Something goes wrong!");
        }
    }

    @Test
    public void testChoice() throws Exception {
        Automata obj = new Automata();
        obj.on();
        obj.coin(100);
        assertSame("CHECK", obj.choice());
    }

    @Test
    public void testCheck() throws Exception {
        Automata obj = new Automata();
        int cash = 20;
        int indexofdrink = 1;
        assertEquals(true, obj.check(cash, indexofdrink));
    }

    @Test
    public void testCancel() throws Exception {
        Automata obj = new Automata();
        obj.on();
        obj.coin(100);
        assertSame("WAIT", obj.cancel());
    }

    @Test
    public void cook() throws Exception {
        Automata obj = new Automata();
        obj.on();
        obj.coin(100);
        obj.choice();
        assertSame("COOK", obj.cook());
    }

    @Test
    public void change() throws Exception {
        Automata obj = new Automata();
        assertEquals(0, obj.change());
    }

}
