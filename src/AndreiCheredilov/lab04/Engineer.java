package Andrei;

public class Engineer extends Employee implements IWorkTime, IProject {

    int base = 0;
    String project;
    double projectContribution;
    int projectBudget;

    Engineer(int id, String name, int base, String project, int projectBudget, double projectContribution) {
        super(id, name);
        this.base = base;
        this.project = project;
        this.projectContribution = projectContribution;
        this.projectBudget = projectBudget;
    }

    public double calcWorkTimePayment(int hour, int base) {

        return hour * base;
    }

    public double calcProjectPayment(int projectBudget, double projectContribution) {
        return projectBudget * projectContribution;
    }

    @Override
    public void salaryCalculator() {
        this.setPayment((int)(calcWorkTimePayment(worktime, base) +
                calcProjectPayment(projectBudget, projectContribution)));
    }
}
