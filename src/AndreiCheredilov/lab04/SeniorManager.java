package Andrei;

class SeniorManager extends ProjectManager {

    SeniorManager(int id, String name, String project, int projectBudget, double projectContribution,
                  int sizeOfTeam, int rateOfLeading){
        super(id,name,project,projectBudget,projectContribution,sizeOfTeam,rateOfLeading);
    }
    public void salaryCalculator() {
        setPayment(calcProjectPayment(projectBudget,projectContribution) +
                calcHeadingPayment(sizeOfTeam, rateOfLeading));
    }
}
