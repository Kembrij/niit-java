package Andrei;

public class Project {
    protected String name;
    protected int projectBudget;


    public static final double rateForSeniorManager = 0.12;
    public static final double rateForProjectManager = 0.09;
    public static final double rateForTeamLeader = 0.029;
    public static final double rateForProgrammer = 0.018;
    public static final double rateForTester = 0.014;

    Project(String name, int projectBudget){
        this.name = name;
        this.projectBudget = projectBudget;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getProjectBudget() {
        return projectBudget;
    }

    public void setProjectBudget(int projectBudget) {
        this.projectBudget = projectBudget;
    }

    public double getRateForSeniorManager() {
        return rateForSeniorManager;
    }
    public double getForProjectManager() {
        return rateForProjectManager;
    }
    public double getRateForTeamLeader() {
        return rateForTeamLeader;
    }
    public double getRateForProgrammer() {
        return rateForProgrammer;
    }
    public double getRateForTester() {
        return rateForTester;
    }

}
