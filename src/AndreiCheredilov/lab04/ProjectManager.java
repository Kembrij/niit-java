package Andrei;

public class ProjectManager extends Manager implements IHeading {
    int sizeOfTeam;
    int rateOfLeading;

    ProjectManager(int id, String name, String project, int projectBudget, double projectContribution,
                   int sizeOfTeam, int rateOfLeading){
        super(id, name, project, projectBudget, projectContribution);
        this.sizeOfTeam = sizeOfTeam;
        this.rateOfLeading = rateOfLeading;
    }

    @Override
    public double calcHeadingPayment(int sizeOfTeam, int rateOfLeading) {
        return sizeOfTeam*rateOfLeading;
    }

    public void salaryCalculator() {
        setPayment(calcProjectPayment(projectBudget,projectContribution) +
                calcHeadingPayment(sizeOfTeam, rateOfLeading));
    }
}
