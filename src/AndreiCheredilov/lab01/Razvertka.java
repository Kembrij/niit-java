public class Razvertka {
    public static void main(String args[]) {
        int i = 0;
        int prev = 0;
        int next = 0;
        char array[] = args[0].toCharArray();
        while (i <array.length){
            if (Character.isDigit(array[i])) {
                prev = (Character.digit(array[i], 10)) + prev * 10;

            } else if (array[i] == ',') {
                System.out.print(prev + ",");
                prev = 0;

            } else {
                i++;
                while (i < array.length && Character.isDigit(array[i])) {
                    next = (Character.digit(array[i], 10)) + next * 10;
                    i++;
                }
                while (prev < next) {
                    System.out.print(prev + ",");
                    prev++;
                }
            }
            i++;
        }
        System.out.println(prev);
    }
}
